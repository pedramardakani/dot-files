#
# ~/.bashrc
#

# If not running interactively, don't do anything
[[ $- != *i* ]] && return

# Inspired from Mohammad's gitlab
#
# Add the Git branch information to the command prompt only when Git
# is present. Also set the command-prompt color to purple for normal
# users and red when the root is running it.
#
# https://gitlab.com/makhlaghi/operating-system-customization/-/tree/master/

if git --version &> /dev/null; then
    parse_git_branch() {
        git branch 2> /dev/null | awk '$1=="*" { print " ("$2")"}'
    }
else
    parse_git_branch() { echo &> /dev/null; }
fi

if [ x$(whoami) = xroot ]; then
    export PS1="\[\033[01;31m\]\u@\h \W\[\033[32m\]\$(parse_git_branch)\[\033[00m\]# "
else
    export PS1="\[\033[01;35m\]\u@\h:\W\[\033[32m\]\$(parse_git_branch)\[\033[00m\]$ "
fi





# Aliases
alias ls='ls --color=auto'
alias scrcpy="scrcpy -b 3M"
alias grep='grep --color=auto'
alias l='ls --color=auto -ltravh --ignore=.?*'
alias ll='ls --color=auto -ltravh --ignore=..'





[[ "$(whoami)" = "root" ]] && return

# Limit recursive functions, see 'man bash'
[[ -z "$FUNCNEST" ]] && export FUNCNEST=100

# Use 'Ctrl+l' as a shortcut to 'reset' instead of the default 'clear' to
# prevent 'tmux' from actually clearing the entire history from current
# terminal session.
bind -x $'"\C-l":reset;'

## Use the up and down arrow keys for finding a command in history
## (you can write some initial letters of the command first).
bind '"\e[B":history-search-forward'
bind '"\e[A":history-search-backward'

# TeXLive
export PATH="$HOME/.local/texlive/2022/bin/x86_64-linux:$PATH"
export MANPATH="$HOME/.local/texlive/2022/texmf-dist/doc/man:$MANPATH"
export INFOPATH="$HOME/.local/texlive/2022/texmf-dist/doc/info:$INFOPATH"

# CFITSIO
export LD_LIBRARY_PATH="/usr/local/lib:$LD_LIBRARY_PATH"

# Gnuasro
export PATH="$HOME/.local/gnuastro/bin:$PATH"
export INFOPATH="$HOME/.local/gnuastro/share/info:$INFOPATH"

# Set unlimited bash history and remove duplications
export HISTSIZE=
export HISTFILESIZE=
export HISTCONTROL=ignoreboth:erasedups
